#!/usr/bin/env bash

#install services and apps
apt-get update
apt-get install -y joe acl git

#prepare local directories
mkdir /home/vagrant/lib
mkdir /home/vagrant/bin


sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password vagrant'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password vagrant'
sudo apt-get -y install mysql-server mysql-client

sudo apt-get install -y apache2
sudo apt-get install -y libapache2-mod-php5 php5 php5-common php5-curl php5-dev php5-gd \
php5-imagick php5-mcrypt php5-memcache php5-mhash php5-mysql php5-pspell php5-intl \
php5-sqlite php5-xmlrpc php5-xsl

#link symfony web dir to www (apache virtualhost directory)
#if ! [ -L /var/www ]; then
#  rm -rf /var/www
#  ln -fs /vagrant/web /var/www
#fi

#composer install
curl -sS https://getcomposer.org/installer | php -- --install-dir=/home/vagrant/lib/
ln -sf /home/vagrant/lib/composer.phar /home/vagrant/bin/composer

#install npm, bower, less
sudo apt-get install -y npm
#sudo chown -R $(whoami) ~/.npm

#mkdir "${HOME}/.npm-packages"
#echo "NPM_PACKAGES=\"\${HOME}/.npm-packages\"" >> ~/.bashrc
#echo "NODE_PATH=\"\$NPM_PACKAGES/lib/node_modules:\$NODE_PATH\"" >> ~/.bashrc

#echo "prefix=\${HOME}/.npm-packages" >> ~/.npmrc
#source ~/.bashrc

#needed for bower (nodejs is not used by bower)
sudo ln /usr/bin/nodejs /usr/bin/node

#install bower, less, gulp
npm install -g bower
npm install -g less
npm install -g gulp


#set virtualhost file to sites-available and enable site
sudo cp -rf /var/www/apache/local.ieag.conf /etc/apache2/sites-available/000-default.conf
sudo apachectl restart
cwd /var/www/
npm install gulp-less
npm install event-stream
#/home/vagrant/bin/composer install


# load mysql data
#cat /var/www/dbdump/somesql.sql | mysql -u root -pvagrant

